package dao.ExceptionDAO;

public class ConfigurationDAOException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ConfigurationDAOException(String message){
		super(message);
	}
	
	public ConfigurationDAOException(String message, Throwable cause){
		super(message,cause);
	}
	
	public ConfigurationDAOException(Throwable cause){
		super(cause);
	}
}
