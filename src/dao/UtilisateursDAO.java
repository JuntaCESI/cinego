package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import dao.ExceptionDAO.DAOException;
import metier.Cinema;
import metier.Film;
import metier.Reservation;
import metier.Utilisateur;

public class UtilisateursDAO implements IUtilisateursDAO {
	
	private EntityManager eM;

	public UtilisateursDAO(EntityManager eM) {
		this.eM=eM;
	}

	@Override
	public Utilisateur saveOrUpdate(Utilisateur utilisateur) {

		if (utilisateur == null)
			return null;

		try {

			eM.getTransaction().begin();
			if (eM.find(Utilisateur.class, utilisateur.getIdUtilisateur()) == null) {
				eM.persist(utilisateur);
			} else if (eM.find(Utilisateur.class, utilisateur.getIdUtilisateur()) != null) {
				eM.merge(eM.find(Utilisateur.class, utilisateur.getIdUtilisateur()));
			}
			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur saveOrUpdate Utilisateur", e);
		} finally {
		}
		return utilisateur;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Utilisateur> getAll() {

		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();

		try {

			eM.getTransaction().begin();

			utilisateurs = eM.createQuery("from Utilisateur").getResultList();

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur getALL Utilisateurs", e);
		} finally {
		}
		return utilisateurs;
	}

	@Override
	public boolean remove(Utilisateur utilisateur) {

		boolean removed = false;

		if (utilisateur == null)
			return removed;

		try {

			eM.getTransaction().begin();

			if (eM.find(Utilisateur.class, utilisateur.getIdUtilisateur()) != null) {
				eM.remove(eM.find(Utilisateur.class, utilisateur.getIdUtilisateur()));
				removed = true;
			}

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur remove Utilisateur", e);
		} finally {
		}
		return removed;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean removeAll() {

		boolean removed = true;

		try {

			eM.getTransaction().begin();

			eM.createQuery("from Utilisateur").getResultList()
			.forEach(utilisateur -> eM.remove(utilisateur));

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			removed = false;
			throw new DAOException("Erreur removeAll Utilisateurs", e);
		} finally {
		}
		return removed;
	}

	@Override
	public Utilisateur findById(int idUtilisateur) {

		Utilisateur utilisateur = null;

		try {

			eM.getTransaction().begin();

			utilisateur = eM.find(Utilisateur.class, idUtilisateur);

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur findById idUtilisateur", e);
		} finally {
		}
		return utilisateur;
	}

	@Override
	public List<Reservation> getReservations(int idUtilisateur) {

		List<Reservation> reservations = new ArrayList<Reservation>();

		try {

			eM.getTransaction().begin();

			reservations = eM
					.createQuery("select r from Reservation r where r.client.idUtilisateur =:idUtilisateur",Reservation.class)
					.setParameter("idUtilisateur", idUtilisateur)
					.getResultList();

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur getReservations idUtilisateur", e);
		} finally {
		}
		return reservations;
	}

	@Override
	public List<Film> getFilms(int idUtilisateur) {

		List<Film> films = new ArrayList<Film>();

		try {

			eM.getTransaction().begin();

			films = eM
					.createQuery("select f "
							+ "from Film f "
							+ "inner join f.séances s "
							+ "inner join s.reservations r "
							+ "where r.client.idUtilisateur =:idUtilisateur",Film.class)
					.setParameter("idUtilisateur", idUtilisateur)
					.getResultList();

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur getFilms idUtilisateur", e);
		} finally {
		}
		return films;
	}

	@Override
	public List<Cinema> getCinemas(int idUtilisateur) {

		List<Cinema> cinemas = new ArrayList<Cinema>();

		try {

			eM.getTransaction().begin();

			cinemas = eM
					.createQuery("select distinct c "
							+ "from Cinema c, Salle s, Séance se "
							+ "where s.cinema.idCinema=c.idCinema and "
							+ "s.idSalle=se.salle.idSalle and "
							+ ":idUtilisateur= some( "
							+ "select u.idUtilisateur "
							+ "from Utilisateur u "
							+ "inner join u.reservations r "
							+ "where r.séance.idSéance=se.idSéance)",Cinema.class)
					.setParameter("idUtilisateur", idUtilisateur)
					.getResultList();

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur getCinemas idUtilisateur", e);
		} finally {
		}
		return cinemas;
	}

}
