package dao;

import java.util.List;

import metier.Cinema;
import metier.Film;
import metier.Reservation;
import metier.Utilisateur;

public interface IUtilisateursDAO {

	Utilisateur saveOrUpdate(Utilisateur utilisateur);

	List<Utilisateur> getAll();

	boolean remove(Utilisateur utilisateur);

	boolean removeAll();

	Utilisateur findById(int idUtilisateur);

	List<Reservation> getReservations(int idUtilisateur);
	
	List<Film> getFilms(int idUtilisateur);
	
	List<Cinema> getCinemas(int idUtilisateur);
	

}
