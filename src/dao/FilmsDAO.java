package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import dao.ExceptionDAO.DAOException;
import metier.Film;

public class FilmsDAO implements IFilmDAO {

	private EntityManager eM;

	public FilmsDAO(EntityManager eM) {
		this.eM=eM;
	}

	@Override
	public Film saveOrUpdate(Film film) {

		if (film == null)
			return null;

		try {

			eM.getTransaction().begin();
			if (eM.find(Film.class, film.getIdFilm()) == null) {
				eM.persist(film);
			} else if (eM.find(Film.class, film.getIdFilm()) != null) {
				eM.merge(eM.find(Film.class, film.getIdFilm()));
			}
			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur saveOrUpdate Film", e);
		} finally {
		}

		return film;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Film> getAll() {

		List<Film> films = new ArrayList<Film>();

		try {

			eM.getTransaction().begin();

			films = eM.createQuery("from Film").getResultList();

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur getAll Films", e);
		} finally {
		}
		return films;
	}

	@Override
	public boolean remove(Film film) {

		boolean removed = false;

		if (film == null)
			return removed;

		try {

			eM.getTransaction().begin();

			if (eM.find(Film.class, film.getIdFilm()) != null) {
				eM.remove(eM.find(Film.class, film.getIdFilm()));
				removed = true;
			}

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur remove Film", e);
		} finally {
		}
		return removed;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean removeAll() {

		boolean removed = true;

		try {

			eM.getTransaction().begin();

			eM.createQuery("from Film").getResultList()
			.forEach(film -> eM.remove(film));

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			removed = false;
			throw new DAOException("Erreur removeAll Films", e);
		} finally {
		}
		return removed;
	}

	@Override
	public Film findById(int idFilm) {

		Film film = null;

		try {

			eM.getTransaction().begin();

			film = eM.find(Film.class, idFilm);

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur findById idFilm", e);
		} finally {
		}
		return film;
	}

	@Override
	public List<Film> findByTitre(String titreFilm) {

		List<Film> films = new ArrayList<Film>();

		try {

			eM.getTransaction().begin();

			films = eM
					.createQuery("select f from Film f where f.titre =:titreFilm",Film.class)
					.setParameter("titreFilm", titreFilm)
					.getResultList();

			eM.getTransaction().commit();
		} catch (Exception e) {
			if (eM.isOpen())
				eM.getTransaction().rollback();
			throw new DAOException("Erreur findByTitre titreFilm", e);
		} finally {
		}
		return films;
	}

	public EntityManager geteM() {
		return eM;
	}

}
