package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dao.ExceptionDAO.ConfigurationDAOException;

public class EntityManagerUtil {

	private static final String PERSISTENCE_UNIT="gregPersist";
	

	public static EntityManager getEntityManager() {
		
		EntityManagerFactory emf;
		
		try{
			emf=Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
		}catch(Throwable e){
			throw new ConfigurationDAOException("Impossible de créer un EntityManager avec le persistence-unit  "+PERSISTENCE_UNIT,e);
		}
		return emf.createEntityManager();
	}
}
