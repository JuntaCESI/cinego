package dao;

import javax.persistence.EntityManager;

import dao.ExceptionDAO.ConfigurationDAOException;

public class FactoryDAO {

	private EntityManager eM;

	private static FactoryDAO instance=null;


	private FactoryDAO(EntityManager eM){
		this.eM=eM;
	}

	public static FactoryDAO getInstance()throws ConfigurationDAOException{
		
		if(instance==null){
			instance=new FactoryDAO(EntityManagerUtil.getEntityManager());
		}
		
		return instance;
	}

	public FilmsDAO getFilmsDAO(){
		return new FilmsDAO(eM);
	}

	public ReservationsDAO getReservationsDAO(){
		return new ReservationsDAO(eM);
	}

	public UtilisateursDAO getUtilisateursDAO(){
		return new UtilisateursDAO(eM);
	}


}
