package dao;

import java.util.List;

import metier.Film;

public interface IFilmDAO {

	Film saveOrUpdate(Film film);

	List<Film> getAll();

	boolean remove(Film film);

	boolean removeAll();

	Film findById(int idFilm);

	List<Film> findByTitre(String titreFilm);

}
