package dao;

import java.util.List;

import metier.Reservation;

public interface IReservationsDAO {

	Reservation saveOrUpdate(Reservation reservation);

	List<Reservation> getAll();

	boolean remove(Reservation reservation);

	boolean removeAll();

	Reservation findById(int idReservation);

	List<Reservation> findBySeance(int idSeance);

	List<Reservation> findByClient(int idClient);
	
	List<Reservation> findByFilm(int idFilm);
	
	List<Reservation> findByCinema(int idCinema);

}
