package metier;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SALLE")
public class Salle {

	@Id
	@Column(name = "SALLE_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idSalle;

	@Column(name = "SALLE_NUMERO")
	private int numéro;

	@Column(name = "SALLE_NOMBRERANGEE")
	private int nbRangées;

	@Column(name = "SALLE_NOMBRESIEGERANGEE")
	private int nbSiegeRangées;

	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,
			CascadeType.REFRESH })
	@JoinColumn(name = "cinema", referencedColumnName = "CINEMA_ID", nullable = false, insertable = true, updatable = true)
	private Cinema cinema;

	public Salle() {
		super();
	}

	public Salle(int idSalle, int numéro, int nbRangées, int nbSiegeRangées,
			Cinema cinema) {
		super();
		this.idSalle = idSalle;
		this.numéro = numéro;
		this.nbRangées = nbRangées;
		this.nbSiegeRangées = nbSiegeRangées;
		this.cinema = cinema;
	}

	public int getIdSalle() {
		return idSalle;
	}

	public void setIdSalle(int idSalle) {
		this.idSalle = idSalle;
	}

	public int getNuméro() {
		return numéro;
	}

	public void setNuméro(int numéro) {
		this.numéro = numéro;
	}

	public int getNbRangées() {
		return nbRangées;
	}

	public void setNbRangées(int nbRangées) {
		this.nbRangées = nbRangées;
	}

	public int getNbSiegeRangées() {
		return nbSiegeRangées;
	}

	public void setNbSiegeRangées(int nbSiegeRangées) {
		this.nbSiegeRangées = nbSiegeRangées;
	}

	public Cinema getCinema() {
		return cinema;
	}

	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cinema == null) ? 0 : cinema.hashCode());
		result = prime * result + idSalle;
		result = prime * result + nbRangées;
		result = prime * result + nbSiegeRangées;
		result = prime * result + numéro;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Salle other = (Salle) obj;
		if (cinema == null) {
			if (other.cinema != null)
				return false;
		} else if (!cinema.equals(other.cinema))
			return false;
		if (idSalle != other.idSalle)
			return false;
		if (nbRangées != other.nbRangées)
			return false;
		if (nbSiegeRangées != other.nbSiegeRangées)
			return false;
		if (numéro != other.numéro)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String
				.format("%s {\n\tidSalle: %s\n\tnuméro: %s\n\tnbRangées: %s\n\tnbSiegeRangées: %s\n\tcinema: %s\n}",
						getClass().getName(), idSalle, numéro, nbRangées,
						nbSiegeRangées, cinema);
	}

}
