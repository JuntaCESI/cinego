package appli;

import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Timer;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.junit.Assert;

import dao.EntityManagerUtil;
import dao.FactoryDAO;
import dao.FilmsDAO;
import dao.ReservationsDAO;
import dao.SessionProvider;
import dao.UtilisateursDAO;
import metier.Cinema;
import metier.Film;
import metier.Reservation;
import metier.Salle;
import metier.Séance;
import metier.Utilisateur;

public class testCinego {

	public static void main(String[] args) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {

		FilmsDAO dao = FactoryDAO.getInstance().getFilmsDAO();
		
		ReservationsDAO resDao=FactoryDAO.getInstance().getReservationsDAO();
		
		UtilisateursDAO utilsDao=FactoryDAO.getInstance().getUtilisateursDAO();
		
		Instant before,after;
		
		int i;
		for(i=0;i<10;i++){
			Séance seance = new Séance();
			Film film = new Film();
			film.setTitre("titre"+i);
			film.addSéance(seance);
			
			before=Instant.now();
			dao.saveOrUpdate(film);
			after=Instant.now();
			System.out.println("Durée saveOrUpdate de 1 film en ms: "+Duration.between(before, after));
		}
		
		before=Instant.now();
		List<Film> films = dao.getAll();
		after=Instant.now();
		System.out.println("Durée getAll() de 1000 film en ms: "+Duration.between(after, before));
		
		
		films.forEach(film -> System.out.println(film.getTitre()));
		
		before=Instant.now();
		System.out.println(dao.findByTitre("titre2"));
		after=Instant.now();
		System.out.println("Film Find by titre en ms: "+Duration.between(after, before));
		
		before=Instant.now();
		System.out.println(dao.findByTitre("titre10000"));
		after=Instant.now();
		System.out.println("Film Find by titre inexistant en ms: "+Duration.between(after, before));
		
		List<Film> films1=dao.findByTitre("titre1");
		films1.addAll(dao.findByTitre("titre2"));
		films1.addAll(dao.findByTitre("titre3"));
		films1.addAll(dao.findByTitre("titre4"));
		
		films1.forEach(film->{
			Instant before1=Instant.now();
			dao.remove(film);
			Instant after1=Instant.now();
			System.out.println("Film remove 1 film en ms: "+Duration.between(after1, before1));
		});
		
		Utilisateur client1=new Utilisateur();
		Utilisateur client2=new Utilisateur();
		Utilisateur client3=new Utilisateur();
		Séance seance1=new Séance();
		Séance seance2=new Séance();
		Séance seance3=new Séance();
		Reservation reservation1=new Reservation();
		Reservation reservation2=new Reservation();
		Reservation reservation3=new Reservation();
		Film film1=new Film();
		Salle salle1=new Salle();
		Salle salle2=new Salle();
		Salle salle3=new Salle();
		Cinema cinema1=new Cinema();
		Cinema cinema2=new Cinema();
		
		salle1.setCinema(cinema1);
		salle2.setCinema(cinema2);
		salle3.setCinema(cinema2);
		seance1.setSalle(salle1);
		seance2.setSalle(salle2);
		seance3.setSalle(salle3);
		film1.addSéance(seance1);
		film1.addSéance(seance2);
		film1.addSéance(seance3);
		seance1.addReservation(reservation1);
		seance2.addReservation(reservation2);
		seance3.addReservation(reservation3);
		client1.addReservation(reservation1);
		client2.addReservation(reservation2);
		client1.addReservation(reservation3);
		

		EntityManager eM=EntityManagerUtil.getEntityManager();
		eM.getTransaction().begin();
		eM.persist(cinema1);
		eM.persist(cinema2);
		eM.persist(salle1);
		eM.persist(salle2);
		eM.persist(salle3);
		eM.persist(film1);
//		eM.persist(seance1);
//		eM.persist(reservation1);
		eM.persist(client1);
		eM.persist(client2);
		eM.persist(client3);
		
		System.out.println(client1);
		System.out.println(reservation1);
		System.out.println(cinema1);
		System.out.println(seance1);
		System.out.println(salle1);
		
		eM.getTransaction().commit();
		
		resDao.saveOrUpdate(reservation1);
		resDao.getAll().forEach(res->System.out.println(res));
		System.out.println(resDao.findById(reservation1.getIdReservation()));
		System.out.println(reservation1.getIdReservation());
//		System.out.println(resDao.remove(reservation1));
		
		System.out.println("res findByFilm: "+resDao.findByFilm(25));
		System.out.println("res findByCinema: "+resDao.findByCinema(21));
		System.out.println("res findByClient: "+resDao.findByClient(30));
		
		utilsDao.saveOrUpdate(client3);
		utilsDao.getAll().forEach(utils->System.out.println("utils getAll: "+utils));
		System.out.println("utils findById: "+utilsDao.findById(client3.getIdUtilisateur()));
		System.out.println("client3 getId: "+client3.getIdUtilisateur());
		
		before=Instant.now();
		System.out.println("utils getReservations: "+utilsDao.getReservations(10));
		after=Instant.now();
		System.out.println("utils getReservations durée en s: "+Duration.between(before, after));
		
		System.out.println("utils getFilms: ");
		before=Instant.now();
		utilsDao.getFilms(10).forEach(film->System.out.println(film.getIdFilm()));
		after=Instant.now();
		System.out.println("utils getFilms durée en s: "+Duration.between(before, after));
		
		before=Instant.now();
		System.out.println("utils getCinema: "+utilsDao.getCinemas(20));
		after=Instant.now();
		System.out.println("utils getCinemas durée en s: "+Duration.between(before, after));
		
//		Assert.assertTrue("Film4 est retiré", dao.remove(film4));
//		Assert.assertFalse(dao.geteM().contains(film4));
//		System.out.println(film4);
//		Assert.assertTrue("Seances de film4 est une List vide", film4
//				.getSéances().size() == 0);
//		System.out.println(film4.getSéances());
		
//		Séance seance1 = new Séance();
//		Séance seance2 = new Séance();
//		Séance seance3 = new Séance();
//		
//		Utilisateur client1=new Utilisateur();
//		Utilisateur client2=new Utilisateur();
//		Utilisateur client3=new Utilisateur();
//
//		Film film1 = new Film();
//		film1.setTitre("titre1");
//		film1.addSéance(seance1);
//		dao.saveOrUpdate(film1);
//
//		Film film2 = new Film();
//		film2.setTitre("titre2");
//		film2.addSéance(seance2);
//		dao.saveOrUpdate(film2);
//
//		Film film3 = new Film();
//		film3.setTitre("titre3");
//		film3.addSéance(seance3);
//		dao.saveOrUpdate(film3);
//
//		Film film4 = new Film();
//		film4.setTitre("titre4");
//		dao.saveOrUpdate(film4);
//
//		Film film5 = new Film();
//		film1.setTitre("titre5");
//
//		film1.removeSéance(seance1);
//		dao.saveOrUpdate(film1);

//		List<Film> films = dao.getAll();
//		films.forEach(film -> System.out.println(film.getTitre()));
//
//		Assert.assertTrue("Film4 est retiré", dao.remove(film4));
//		Assert.assertFalse(dao.geteM().contains(film4));
//		System.out.println(film4);
//		Assert.assertTrue("Seances de film4 est une List vide", film4
//				.getSéances().size() == 0);
//		System.out.println(film4.getSéances());

		// System.out.println(dao.remove(film5));//false

		// System.out.println(dao.removeAll());

//		System.out.println(dao.findByTitre("titre2"));
		


		// Session session=new SessionProvider().getSession();
		//
		// session.getTransaction().begin();
		//
		// Cinema cinema = new Cinema();
		// session.persist(cinema);
		// Cinema cinema2 = new Cinema();
		// session.persist(cinema2);
		// Cinema cinema3 = new Cinema();
		// session.persist(cinema3);
		//
		// Utilisateur client1=new Utilisateur();
		// session.persist(client1);
		// Utilisateur client2=new Utilisateur();
		// session.persist(client2);
		// Utilisateur client3=new Utilisateur();
		// session.persist(client3);
		//
		//
		//
		// Film film1=new Film();
		// Film film2=new Film();
		// Film film3=new Film();
		//
		// Séance seance1=new Séance();
		// Séance seance2=new Séance();
		// Séance seance3=new Séance();
		// Séance seance4=new Séance();
		//
		// Salle salle1=new Salle();
		// Salle salle2=new Salle();
		// Salle salle3=new Salle();
		// Salle salle4=new Salle();
		//
		// salle1.setCinema(cinema);
		// salle2.setCinema(cinema2);
		// salle3.setCinema(cinema2);
		// salle4.setCinema(cinema3);
		//
		// session.persist(salle1);
		// session.persist(salle2);
		// session.persist(salle3);
		// session.persist(salle4);
		//
		// film1.addSéance(seance1);
		// film2.addSéance(seance2);
		// film2.addSéance(seance3);
		// film3.addSéance(seance4);
		//
		// seance1.setSalle(salle1);
		// seance2.setSalle(salle2);
		// seance3.setSalle(salle3);
		// seance4.setSalle(salle4);
		//
		// session.persist(film1);
		// session.persist(film2);
		// session.persist(film3);
		//
		// Reservation reservation1=new Reservation();
		// Reservation reservation2=new Reservation();
		// Reservation reservation3=new Reservation();
		//
		// seance1.addReservation(reservation1);
		// client1.addReservation(reservation1);
		// session.merge(client1);
		//
		// seance2.addReservation(reservation2);
		// client2.addReservation(reservation2);
		// session.merge(client2);
		//
		// seance3.addReservation(reservation3);
		// client1.addReservation(reservation3);
		// session.merge(client1);

		/*
		 * // Reservation reservation1=new Reservation(); // Reservation
		 * reservation2=new Reservation(); //
		 * client2.addReservation(reservation2); //
		 * séance2.addReservation(reservation2); //
		 * client1.addReservation(reservation1); //
		 * séance1.addReservation(reservation1); // session.merge(client1); //
		 * session.merge(client2); // // //
		 * System.out.println("Add 2 séances au film:\n"+film1.toString()); //
		 * // film1.removeSéance(séance2); // session.persist(film1); // //
		 * System.out.println("Remove 1 séance du film:\n"+film1.toString()); //
		 * // int idFilm=film1.getIdFilm(); // //
		 * System.out.println("Le cinéma de la salle de la séance:\n"
		 * +séance1.getSalle().getCinema().getIdCinema()); //
		 * System.out.println("Les séances du film:\n"+film1.getSéances()); //
		 * //
		 * System.out.println("Le cinéma de la salle1:\n"+salle1.getCinema());
		 * //
		 * System.out.println("Le cinéma de la salle2(le même):\n"+salle2.getCinema
		 * ());
		 */
		// session.getTransaction().commit();
		// session.flush();

		// Film filmFromBase=session.get(Film.class, idFilm);
		// filmFromBase.getSéances()
		// .forEach((seance)->System.out.println(seance));

		// session.close();
	}
}
