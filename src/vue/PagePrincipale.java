package vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class PagePrincipale extends JFrame {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PagePrincipale() {
		
		super("Cinego");

		setSize(900,900);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		// cr�ation des panels de la page principale
		
		JPanel Content = new JPanel();
		JPanel p = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel(new GridBagLayout());
		JPanel P4 = new JPanel ();
		JPanel choixFilm = new JPanel(new GridBagLayout());
		CardLayout cl = new CardLayout();
		
		Content.setLayout(cl);
	
		
		// Ajout de composants sur le PanelSud
		JLabel copyright = new JLabel("Copyright Cesi Cinego");
		p.add(copyright);
		
		// Ajout de composants sur le panelNord
		JLabel Bienvenue = new JLabel("Bienvenue dans votre cinema !!!!");
		p2.add(Bienvenue);
		
		
		// Ajouts de composants dans le panel central, avec cr�ation d'un GridLayout
		JButton start = new JButton("Appuyer ici pour faire votre r�servation");
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(15, 15, 15, 15);
		p3.add(start,gbc);
		
		
		// ajouts panel 4 d'une image
		JLabel image = new JLabel( new ImageIcon( "C:\\Users\\Joshua\\Downloads\\la-mome-affiche.jpg"));
		P4.add(image);
		
		JButton revenir = new JButton("Page Principale");
		JLabel nom = new JLabel("votre nom");
		JLabel prenom = new JLabel("votre pr�nom");
		
		int w=2, h=2;
        JButton[][] bouton = new JButton[h][w];
        
        for(int i=0; i<h; i++){
            for(int j=0; j<w; j++){
            	
            	
            	
            	
            	gbc.gridx = j;
            	gbc.gridy = i;
            	
            	choixFilm.add(bouton[i][j]=new JButton("case["+i+"]["+j+"]"),gbc);
		
            }
        }
		
        gbc.gridx = 4;
    	gbc.gridy = 4;
		choixFilm.add(revenir,gbc);
		
		choixFilm.add(nom,gbc);
		choixFilm.add(prenom,gbc);
		
		
		
		Content.add(p3,"1");
		Content.add(choixFilm, "2");
		
		cl.show(Content,"1");
		
		
		
		
		
		
		
		
		start.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				cl.show(Content, "2");
				
				
			}
		});
		
		revenir.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				cl.show(Content, "1");
				
			}
		});
		
		
		// Mise en place des panels sur la frame.
		add(p,BorderLayout.SOUTH);
		add(Content,BorderLayout.CENTER);
		add(p2, BorderLayout.NORTH);
		add(P4,BorderLayout.WEST);
		
		
		
		validate();
	}

}
